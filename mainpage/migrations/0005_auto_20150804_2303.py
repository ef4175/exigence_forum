# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainpage', '0004_auto_20150804_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='siteuser',
            name='picture',
            field=models.FileField(upload_to='', blank=True, null=True),
        ),
    ]
