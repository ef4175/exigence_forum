# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainpage', '0002_auto_20150803_1333'),
    ]

    operations = [
        migrations.AddField(
            model_name='thread',
            name='last_activity',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
