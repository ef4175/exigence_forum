from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.home_page),
	url(r'^register$', views.register_page),
	url(r'^login$', views.login_page),
	url(r'^logout$', views.logout_user),
	url(r'^watches$', views.watch_page),
	url(r'^pens$', views.pen_page),
	url(r'^programming$', views.programming_page),
	url(r'^academia$', views.academia_page),
	url(r'^(?P<section>.*)/new$', views.new_thread),
	url(r'^thread/(?P<pk>[0-9]+)$', views.show_thread),
	url(r'^post/(?P<pk>[0-9]+)/edit$', views.edit_post),
	url(r'^user/(?P<pk>[0-9]+)$', views.show_user),
	url(r'^user/(?P<pk>[0-9]+)/newpass$', views.change_password),
]