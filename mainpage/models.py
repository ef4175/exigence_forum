from django.db import models
from django.contrib.auth.models import User

class SiteUser(models.Model):
	user = models.OneToOneField(User, related_name = "account_details")
	picture = models.FileField(blank = True, null = True)

	def __str__(self):
		return self.user.username

class Thread(models.Model):
	author = models.ForeignKey("SiteUser", related_name = "threads")
	title = models.CharField(max_length = 64)
	category = models.CharField(max_length = 16, blank = True)
	submit_date = models.DateTimeField(blank = True, null = True)
	last_activity = models.DateTimeField(blank = True, null = True)
	num_views = models.IntegerField()

	def __str__(self):
		return self.title

class Post(models.Model):
	author = models.ForeignKey("SiteUser", related_name = "posts")
	thread = models.ForeignKey("Thread", related_name = "replies")
	submit_date = models.DateTimeField(blank = True, null = True)
	body = models.TextField()

	def __str__(self):
		return self.thread.title