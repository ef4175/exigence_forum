from django import forms
from .models import SiteUser, Thread, Post

class LoginForm(forms.Form):
	username = forms.CharField(widget = forms.TextInput(attrs = {'style': 'height: 20px; width: 290px;'}))
	password = forms.CharField(widget = forms.PasswordInput(attrs = {'style': 'height: 20px; width: 290px;'}))

class RegistrationForm(forms.Form):
	username = forms.CharField();
	password = forms.CharField(widget = forms.PasswordInput())
	first_name = forms.CharField(label = "First Name")
	last_name = forms.CharField(label = "Last Name")
	email = forms.CharField(label = "E-mail Address")

class ThreadForm(forms.ModelForm):
	class Meta:
		model = Thread
		fields = ['title',]
		widgets = {
			'title': forms.TextInput(attrs = {'style': 'height: 20px; width: 1536px'}),
		}

class PostForm(forms.ModelForm):
	class Meta:
		model = Post
		fields = ['body']
		widgets = {
			'body': forms.Textarea(attrs = {'rows': 20, 'cols': 187})
		}

class ChangePasswordForm(forms.Form):
	new_password = forms.CharField(widget = forms.PasswordInput())