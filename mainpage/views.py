from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from .forms import LoginForm, RegistrationForm, ThreadForm, PostForm, ChangePasswordForm
from .models import SiteUser, Thread, Post
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils import timezone

def home_page(request):
	return render(request, 'mainpage/home_page.html', {})

def login_page(request):
	if request.method == "POST":
		username = request.POST["username"]
		password = request.POST["password"]
		user = authenticate(username = username, password = password)
		if user:
			login(request, user)
			return HttpResponseRedirect('/')
		else:
			login_form = LoginForm()
			return render(request, "mainpage/login_page.html", {'login_form': login_form, 'invalid_login': True})
	else:
		login_form = LoginForm()
	return render(request, "mainpage/login_page.html", {'login_form': login_form})

def register_page(request):
	if "user_register" in request.POST:
		register_form = RegistrationForm(request.POST)
		if register_form.is_valid() and not (User.objects.filter(username = register_form.cleaned_data['username']).exists()) and not (User.objects.filter(email = register_form.cleaned_data['email']).exists()):
			new_user = User()
			new_user.username = register_form.cleaned_data['username']
			new_user.set_password(register_form.cleaned_data['password'])
			new_user.first_name = register_form.cleaned_data['first_name']
			new_user.last_name = register_form.cleaned_data['last_name']
			new_user.email = register_form.cleaned_data['email']
			new_user.save()
			new_site_user = SiteUser()
			new_site_user.user = new_user
			new_site_user.save()
			return redirect("mainpage.views.home_page")
		else:
			register_form = RegistrationForm()
			bad_form = True
	else:
		register_form = RegistrationForm()
		bad_form = False
	return render(request, 'mainpage/register_page.html', {'register_form': register_form, 'bad_form': bad_form})

@login_required
def logout_user(request):
	logout(request)
	return HttpResponseRedirect('/')

def watch_page(request):
	'''
	watch_threads = []
	for thread in Thread.objects.all():
		if thread.category == "watches":
			watch_threads.append(thread)
	'''
	watch_threads = Thread.objects.all().filter(category = "watches").order_by("last_activity").reverse()
	num_threads = len(watch_threads)
	num_posts = 0
	for thread in watch_threads:
		num_posts += len(thread.replies.all())
	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username = username, password = password)
		if user:
			login(request, user)
			return redirect("mainpage.views.watch_page")
		else:
			login_form = LoginForm()
			return render(request, 'mainpage/watch_page.html', {'login_form': login_form, 'invalid_login': True, 'watch_threads': watch_threads, 'num_threads': num_threads, 'num_posts': num_posts})
	else:
		login_form = LoginForm()
	return render(request, 'mainpage/watch_page.html', {'login_form': login_form, 'watch_threads': watch_threads, 'num_threads': num_threads, 'num_posts': num_posts})

def pen_page(request):
	'''
	pen_threads = []
	for thread in Thread.objects.all():
		if thread.category == "pens":
			pen_threads.append(thread)
	'''
	pen_threads = Thread.objects.all().filter(category = "pens").order_by("last_activity").reverse()
	num_threads = len(pen_threads)
	num_posts = 0
	for thread in pen_threads:
		num_posts += len(thread.replies.all())
	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username = username, password = password)
		if user:
			login(request, user)
			return redirect("mainpage.views.pen_page")
		else:
			login_form = LoginForm()
			return render(request, 'mainpage/pen_page.html', {'login_form': login_form, 'invalid_login': True, 'pen_threads': pen_threads, 'num_threads': num_threads, 'num_posts': num_posts})
	else:
		login_form = LoginForm()
	return render(request, 'mainpage/pen_page.html', {'login_form': login_form, 'pen_threads': pen_threads, 'num_threads': num_threads, 'num_posts': num_posts})

def programming_page(request):
	'''
	programming_threads = []
	for thread in Thread.objects.all():
		if thread.category == "programming":
			programming_threads.append(thread)
	'''
	programming_threads = Thread.objects.all().filter(category = "programming").order_by("last_activity").reverse()
	num_threads = len(programming_threads)
	num_posts = 0
	for thread in programming_threads:
		num_posts += len(thread.replies.all())
	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username = username, password = password)
		if user:
			login(request, user)
			return redirect("mainpage.views.programming_page")
		else:
			login_form = LoginForm()
			return render(request, 'mainpage/programming_page.html', {'login_form': login_form, 'invalid_login': True, 'programming_threads': programming_threads, 'num_threads': num_threads, 'num_posts': num_posts})
	else:
		login_form = LoginForm()
	return render(request, 'mainpage/programming_page.html', {'login_form': login_form, 'programming_threads': programming_threads, 'num_threads': num_threads, 'num_posts': num_posts})

def academia_page(request):
	'''
	academia_threads = []
	for thread in Thread.objects.all():
		if thread.category == "academia":
			academia_threads.append(thread)
	'''
	academia_threads = Thread.objects.all().filter(category = "academia").order_by("last_activity").reverse()
	num_threads = len(academia_threads)
	num_posts = 0
	for thread in academia_threads:
		num_posts += len(thread.replies.all())
	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username = username, password = password)
		if user:
			login(request, user)
			return redirect("mainpage.views.academia_page")
		else:
			login_form = LoginForm()
			return render(request, 'mainpage/academia_page.html', {'login_form': login_form, 'invalid_login': True, 'academia_threads': academia_threads, 'num_posts': num_posts, 'num_threads': num_threads})
	else:
		login_form = LoginForm()
	return render(request, 'mainpage/academia_page.html', {'login_form': login_form, 'academia_threads': academia_threads, 'num_posts': num_posts, 'num_threads': num_threads})

@login_required
def new_thread(request, section):
	if "post_form" in request.POST:
		thread_form = ThreadForm(request.POST)
		post_form = PostForm(request.POST)
		if thread_form.is_valid() and post_form.is_valid():
			t = thread_form.save(commit = False)
			t.category = section
			t.submit_date = timezone.now()
			t.last_activity = t.submit_date
			t.num_views = 0
			current_user = get_object_or_404(SiteUser, pk = request.user.pk)
			t.author = current_user
			t.save()
			p = post_form.save(commit = False)
			p.submit_date = timezone.now()
			p.author = current_user
			p.thread = t
			p.save()
			return redirect("mainpage.views.show_thread", pk = t.pk)
			'''
			if section == "watches":
				section = "watch"
			elif section == "pens":
				section = "pen"
			return redirect("mainpage.views." + section + "_page")
			'''
		else:
			thread_form = ThreadForm()
			post_form = PostForm()
	else:
		thread_form = ThreadForm()
		post_form = PostForm()
	return render(request, 'mainpage/new_thread_page.html', {'thread_form': thread_form, 'post_form': post_form})

def show_thread(request, pk):
	thread = get_object_or_404(Thread, pk=pk)
	thread.num_views += 1
	thread.save()
	if "user_login" in request.POST:
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username = username, password = password)
		if user:
			login(request, user)
			return HttpResponseRedirect('/')
		else:
			login_form = LoginForm()
			return render(request, 'mainpage/show_thread.html', {'login_form': login_form, 'invalid_login': True, 'thread': thread})
	elif "post_form" in request.POST:
		post_form = PostForm(request.POST)
		if post_form.is_valid():
			p = post_form.save(commit = False)
			current_user = get_object_or_404(SiteUser, pk = request.user.pk)
			p.author = current_user
			p.thread = thread
			p.submit_date = timezone.now()
			p.save()
			thread.last_activity = p.submit_date
			thread.num_views -= 2      #Do not increment view count if user submits a reply. Decrement by two because loading thread and submitting reply 
			thread.save()              #both add one. Count just the load, not the submission.
			return redirect("mainpage.views.show_thread", pk = thread.pk)
		else:
			login_form = LoginForm()
			post_form = PostForm()
			return render(request, 'mainpage/show_thread.html', {'login_form': login_form, 'thread': thread, 'post_form': post_form})
	else:
		login_form = LoginForm()
		post_form = PostForm()
		return render(request, 'mainpage/show_thread.html', {'login_form': login_form, 'thread': thread, 'post_form': post_form})

@login_required
def edit_post(request, pk):
	post = get_object_or_404(Post, pk = pk)
	if "post_form" in request.POST:
		edit_post_form = PostForm(request.POST, instance = post)
		if edit_post_form.is_valid():
			p = edit_post_form.save(commit = False)
			p.save()
			return redirect("mainpage.views.show_thread", pk = post.thread.pk)
		else:
			edit_post_form = PostForm(instance = post)
			return render(request, 'mainpage/edit_post.html', {'edit_post_form': edit_post_form, 'post': post, 'empty_form': True})
	elif request.user.username == post.author.user.username:
		edit_post_form = PostForm(instance = post)
		return render(request, 'mainpage/edit_post.html', {'edit_post_form': edit_post_form, 'post': post})
	else:
		return redirect("mainpage.views.show_thread", pk = post.thread.pk)

@login_required
def show_user(request, pk):
	current_user = get_object_or_404(SiteUser, pk = pk)
	if "user_login" in request.POST:
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username = username, password = password)
		if user:
			login(request, user)
			return HttpResponseRedirect('/')
		else:
			login_form = LoginForm()
			return render(request, 'mainpage/show_user.html', {'login_form': login_form, 'invalid_login': True, 'thread': thread})
	else:
		login_form = LoginForm()
		return render(request, 'mainpage/show_user.html', {'current_user': current_user, 'login_form': login_form})

@login_required
def change_password(request, pk):
	current_user = get_object_or_404(SiteUser, pk = pk)
	if request.user.pk != current_user.pk:
		return redirect('mainpage.views.home_page')
	elif request.method == "POST":
		password_form = ChangePasswordForm(request.POST)
		if password_form.is_valid():
			current_user = get_object_or_404(User, pk = pk)
			current_user.set_password(password_form.cleaned_data['new_password'])
			current_user.save()
			return redirect("mainpage.views.home_page")
		else:
			password_form = ChangePasswordForm()
			return render(request, 'mainpage/change_password.html', {'password_form': password_form})
	else:
		password_form = ChangePasswordForm()
		return render(request, 'mainpage/change_password.html', {'password_form': password_form})
