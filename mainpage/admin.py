from django.contrib import admin
from .models import SiteUser, Thread, Post

admin.site.register(SiteUser)
admin.site.register(Thread)
admin.site.register(Post)