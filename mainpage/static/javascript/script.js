currentImg = 1;
nextImg = 2;

$(document).ready(function() {
	$("#imageSlider > img#1").fadeIn(500);
	startSlider();
	$("#imageSlider > img").hover(
		function() {
			stopLoop();
		},
		function() {
			startSlider();
		}
	);

	$(".errorlist").hide(); //Hide form errors
	$("#loginForm").hide(); //Hide form when page loads and show only when user hovers.
	$(".editPost").hide();
	$(".newestThreads").css("height", "630px");
	$("#loginDiv").mouseenter(function() {
		$(this).css("border-bottom", "10px solid #8585AD");
		$("#loginForm").show();
	});
	$("#loginDiv").mouseleave(function() {
		$("#loginForm").hide();
		$(this).css("border-bottom", "0px");
	});
	$("#loginForm").mouseenter(function() {
		$("#loginForm").show();
		$("#loginDiv").css("border-bottom", "10px solid #8585AD");
	});
	$("#loginForm").mouseleave(function() {
		$(this).hide();
		$("#loginDiv").css("border-bottom", "0px");
	});

	$("#userControlPanel").hide();
	$("#showUsername").mouseenter(function() {
		$("#userControlPanel").show();
		$(this).css("background-color", "#8585AD");
	});
	$("#showUsername").mouseleave(function() {
		$("#userControlPanel").hide();
		$(this).css("background-color", "transparent");
	});
	$("#userControlPanel").mouseenter(function() {
		$(this).show();
		$("#showUsername").css("background-color", "#8585AD");
	});
	$("#userControlPanel").mouseleave(function() {
		$("#showUsername").css("background-color", "transparent");
		$(this).hide();
	});
	$(".showReply").mouseenter(function() {
		$(this).css("background-color", "#E8E8E8");
		$(this).css("z-index", "60");
	});
	$(".showReply").mouseleave(function() {
		$(this).css("background-color", "#F3F3F3");
		$(this).css("z-index", "1");
	});
	$(".editContainer").mouseenter(function() {
		$(this).children(".editPost").show();
	});
	$(".editContainer").mouseleave(function() {
		$(this).children(".editPost").hide();
	});
});

function startSlider() {
	numImgs = $("#imageSlider > img").size();
	loop = setInterval(function() {
		if (nextImg > numImgs) {
			nextImg = 1;
			currentImg = 1;
		}
		$("#imageSlider > img").fadeOut(500);
		$("#imageSlider > img#" + nextImg).fadeIn(500);
		currentImg = nextImg;
		nextImg = nextImg + 1;
	}, 4000);
}

function showPrevImg() {
	newImg = currentImg - 1;
	showImg(newImg);
}

function showNextImg() {
	newImg = currentImg + 1;
	showImg(newImg);
}

function stopLoop() {
	window.clearInterval(loop);
}

function showImg(imgIndex) {
	stopLoop();
	if (imgIndex > numImgs) {
		imgIndex = 1;
		
	} else if (imgIndex < 1) {
		imgIndex = numImgs;
	}
	$("#imageSlider > img").fadeOut(500);
	$("#imageSlider > img#" + imgIndex).fadeIn(500);
	currentImg = imgIndex;
	nextImg = imgIndex + 1;
	startSlider();
}

$(document).on("click", "#showMoreThreads", function() {
	$(".newestThreads").css("height", "+=620px");
	var y = $(window).scrollTop();
	$(window).scrollTop(y + 620);
});